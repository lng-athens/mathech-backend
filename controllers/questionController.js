const expressAsyncHandler = require('express-async-handler');
const Question = require('../models/Question');
const QuizSet = require('../models/QuizSet');
const auth = require('../middleware/Authentication');
const { connectToDatabase, disconnectToDatabase, getClient } = require('../config/database');
const { ObjectId } = require('mongodb');
const { disconnect } = require('mongoose');

const CreateQuestion = expressAsyncHandler(async (req, res) => {
    const newQuestionData = req.body;

    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const questionCollection = db.collection('questions');
    const filter = {question: newQuestionData.question};

    const isQuestionExist = await questionCollection.findOne(filter);
    if (isQuestionExist) {
        await disconnectToDatabase();
        res.status(400);
        throw new Error('Question already exists!');
    }

    let userId = await auth.decodeToken(req.headers.authorization);
    userId = new ObjectId(userId._id);

    const newQuestion = new Question({
        ...newQuestionData,
        createdBy: userId
    });

    await questionCollection.insertOne(newQuestion);
    await disconnectToDatabase();
    res.status(201).send({message: 'Questions successfully created!'});
});

const GetUserQuestions = expressAsyncHandler(async (req, res) => {
    const UserId = new ObjectId(req.params.id);

    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const questionCollection = db.collection('questions');

    const QuestionList = await questionCollection.find({createdBy: UserId}).toArray();
    if (QuestionList.length === 0) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('No question found!');
    }

    res.status(200).send(QuestionList);
});

const DeleteUserQuestion = expressAsyncHandler(async (req, res) => {
    const questionId = new ObjectId(req.params.id);

    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const questionCollection = db.collection('questions');
    const filter = {_id: questionId};
    
    const questionItem = await questionCollection.findOne(filter);
    if (!questionItem) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('Question does not exists!');
    }

    await questionCollection.deleteOne(filter);
    res.status(204).end();
});

const EditUserQuestion = expressAsyncHandler(async (req, res) => {
    const questionId = new ObjectId(req.params.id);
    const updatedQuestion = req.body;

    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const questionCollection = db.collection('questions');
    const filter = {_id: questionId};

    const questionItem = await questionCollection.findOne(filter);
    if (!questionItem) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('Question does not exists!');
    }

    await questionCollection.updateOne(filter, {$set: updatedQuestion});
    res.status(204).end();
});

module.exports = {
    CreateQuestion,
    GetUserQuestions,
    DeleteUserQuestion,
    EditUserQuestion
};