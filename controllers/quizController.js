const expressAsyncHandler = require('express-async-handler');
const Question = require('../models/Question');
const QuizSet = require('../models/QuizSet');
const Quiz = require('../models/Quiz');
const auth = require('../middleware/Authentication');
const { connectToDatabase, disconnectToDatabase, getClient } = require('../config/database');
const { ObjectId } = require('mongodb');

const CreateQuiz = expressAsyncHandler(async (req, res) => {
    const { subject, topics, testType, items, title, academicYear, instruction} = req.body;
    const userId = new ObjectId(req.params.id);

    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const questionCollection = db.collection('questions');
    const quizSetCollection = db.collection('quiz_set');
    const quizCollection = db.collection('quiz');

    const questions = await questionCollection.aggregate([
        {$match : {
            subject: {$regex: new RegExp(subject, 'i')},
            subTopic: {$in: topics.map(topic => new RegExp(topic, 'i'))},
            questionType: testType.replace(/ /g, '-')
        }}
    ]).toArray();

    for (let i = questions.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [questions[i], questions[j]] = [questions[j], questions[i]];
    }

    let selectedQuestions = questions.slice(0, items);
    selectedQuestions = selectedQuestions.map(question => ({
        question: question.question,
        choices: question.choices,
        answer: question.answer
    }));

    let direction;
    if (testType === 'Problem Solving') {
        direction = `Read and analyze each item. Perform the appropriate analysis and show all necessary solutions. Write your solutions on the space provided or at the back the paper. **No extra sheet of paper is allowed**.`;
    }
    else if (testType === 'Multiple Choice' || testType === 'True or False') {
        direction = `**Encircle** the letter of the correct answer.`;
    }
    else if (testType === 'Modified True or False') {
        direction = `Write **A** if the idea stated is correct and **B** if it is incorrect. If your answer is **B**, write the appropriate term to replace the underlined expression to make the statement correct **beside B**.`
    }
    
    const newQuizSet = new QuizSet({
        setType: testType.replace(/ /g, '-'),
        instruction: direction,
        subject: subject,
        subTopic: topics,
        questions: selectedQuestions
    });

    const generatedQuizSet = await quizSetCollection.insertOne(newQuizSet);
    const quizSetId = generatedQuizSet.insertedId.toString();

    const newQuiz = new Quiz({
        title: title,
        instruction: instruction,
        subject: subject,
        topics: topics,
        quizSet: [quizSetId],
        academicYear: academicYear,
        createdBy: userId,
    });

    await quizCollection.insertOne(newQuiz);
    await disconnectToDatabase();
    res.status(204).end();
});

const ViewUserQuizzes = expressAsyncHandler(async (req, res) => {
    const UserId = new ObjectId(req.params.id);

    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const quizCollection = db.collection('quiz');

    const QuizList = await quizCollection.find({createdBy: UserId}).toArray();
    if (!QuizList) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('No quiz yet');
    }
    
    await disconnectToDatabase();
    res.status(200).send(QuizList);
});

const DeleteUserQuiz = expressAsyncHandler(async (req, res) => {
    const QuizId = new ObjectId(req.params.id);

    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const quizCollection = db.collection('quiz');
    const quizSetCollection = db.collection('quiz_set');

    const QuizItem = await quizCollection.findOne({_id: QuizId});
    if (!QuizItem) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('No quiz found!');
    }

    const QuizSetList = QuizItem.quizSet;

    const DeleteQuizSet = await quizSetCollection.deleteMany({_id: {$in: QuizSetList.map(setId => new ObjectId(setId))}});
    if (DeleteQuizSet.deletedCount === 0) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('No quiz set deleted!');
    }

    const DeleteQuiz = await quizCollection.deleteOne({_id: QuizId});
    if (DeleteQuiz.deletedCount === 0) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('No quiz deleted!');
    }

    await disconnectToDatabase();
    res.status(204).end();
});

const GetFullQuizDetails = expressAsyncHandler(async (req, res) => {
    const QuizId = new ObjectId(req.params.id);
    
    await connectToDatabase();
    const client = getClient();
    const db = client.db(process.env.MONGODB_COLLECTION);
    const quizCollection = db.collection('quiz');
    const quizSetCollection = db.collection('quiz_set');

    const QuizDetails = await quizCollection.findOne({_id: QuizId});
    if (!QuizDetails) {
        await disconnectToDatabase();
        res.status(404);
        throw new Error('Quiz not found!');
    }

    const QuizSetDetails = await quizSetCollection.find({_id: {$in: QuizDetails.quizSet.map(setId => new ObjectId(setId))}}).toArray();

    const FullQuizDetails = ({
        title: QuizDetails.title,
        generalInstruction: QuizDetails.instruction,
        subject: QuizDetails.subject,
        topics: QuizDetails.topics,
        quiz_set: QuizSetDetails,
        academicYear: QuizDetails.academicYear
    });

    await disconnectToDatabase();
    res.status(200).send(FullQuizDetails);
});

module.exports = {
    CreateQuiz,
    ViewUserQuizzes,
    GetFullQuizDetails,
    DeleteUserQuiz
};