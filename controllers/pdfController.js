const PuppeteerHTMLPDF = require('puppeteer-html-pdf');
const hbs = require('handlebars');
const expressAsyncHandler = require('express-async-handler');


const CreatePDF = expressAsyncHandler(async (req, res) => {
    const htmlPDF = new PuppeteerHTMLPDF();
    htmlPDF.setOptions({ format: 'letter' });

    const pdfData = req.body;

    const html = await htmlPDF.readFile(__dirname + '/config/pdfTemplate.html', 'utf8');
    const template = hbs.compile(html);
    const content = template(pdfData);
});

module.exports = {
    CreatePDF
};