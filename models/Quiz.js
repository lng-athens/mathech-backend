const mongoose = require('mongoose');

module.exports = mongoose.model('Quiz', new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Title required'],
    },
    instruction: {
        type: String,
        required: [true, 'General instruction required']
    },
    subject: {
        type: String,
        required: [true, 'Subject required'],
    },
    topics: [
        {
            type: String,
            required: [true, 'Topic required'],
        },
    ],
    quizSet: [
        {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, 'Quiz ID required'],
            ref: "QuizSet",
        },
    ],
    academicYear : {
        type: String,
        required: [true, 'Academic year required'],
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Creator ID required'],
        ref: "User",
    },
}));