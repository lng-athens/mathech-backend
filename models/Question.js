const mongoose = require('mongoose');

module.exports = mongoose.model('Question', new mongoose.Schema({
    question: {
        type: String,
        required: [true, 'Question required'],
    },
    subject: {
        type: String,
        required: [true, 'Subject required'],
    },
    subTopic: {
        type: String,
        required: [true, 'Sub-topic required'],
    },
    questionType: {
        type: String,
        required: [true, 'Question type required'],
    },
    choices: [
        {
            type: String,
        },
    ],
    answer: {
        type: String,
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Creator ID required'],
        ref: "User",
    },
}));