const express = require('express');
const { verifyToken } = require('../middleware/Authentication');
const { CreateQuiz, ViewUserQuizzes, DeleteUserQuiz, GetFullQuizDetails } = require('../controllers/quizController');
const router = express.Router();

router.route('/create/:id').post(verifyToken, CreateQuiz);

router.route('/user/:id').get(verifyToken, ViewUserQuizzes);

router.route('/view/:id').get(verifyToken, GetFullQuizDetails);

router.route('/delete/:id').delete(verifyToken, DeleteUserQuiz);

module.exports = router;