const express = require('express');
const { CreatePDF } = require('../controllers/pdfController');
const router = express.Router();

router.route('/create').post(CreatePDF);

module.exports = router;