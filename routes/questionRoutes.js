const express = require('express');
const { verifyToken } = require('../middleware/Authentication');
const { CreateQuestion, GetUserQuestions, DeleteUserQuestion, EditUserQuestion } = require('../controllers/questionController');
const router = express.Router();

router.route('/create').post(verifyToken, CreateQuestion);

router.route('/userQuestions/:id')
    .patch(verifyToken, EditUserQuestion)
    .get(GetUserQuestions)
    .delete(verifyToken, DeleteUserQuestion);

module.exports = router;